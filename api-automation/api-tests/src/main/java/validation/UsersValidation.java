package validation;

import beans.api.request.users.UsersPostRequest;
import beans.api.request.users.UsersPutRequest;
import beans.api.response.users.UsersGetResponse;
import beans.api.response.users.UsersPostResponse;
import beans.api.response.users.UsersPutResponse;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.MatcherAssert;

import java.util.Objects;
import java.util.function.Function;

import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

/**
 * Created by Marina Buryak 29.10.2018
 */
public class UsersValidation {

    public static void validateUsers(UsersPostResponse response, UsersPostRequest request) {
        String errorMessage = "";
        errorMessage += validate(Objects::nonNull, response, "Response is null");
        errorMessage += validate(r -> 0 == (Integer) r, response.getCode(), "Incorrect code");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getAccesstoken().getAccessToken(), "Empty accesstoken.access_token");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getAccesstoken().getRefreshToken(), "Empty accesstoken.refresh_token");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getAccesstoken().getUserId(), "Empty accesstoken.user_id");

        if(request != null) {
            errorMessage += validate(r -> r.equals(request.getCompanyID()), response.getAccesstoken().getScope(), "Incorrect accesstoken.scope");
        }
        MatcherAssert.assertThat("Incorrect UsersPostResponse response:\n" + errorMessage, errorMessage, isEmptyOrNullString());
    }

    public static void validateUsers(UsersGetResponse response) {
        String errorMessage = "";
        errorMessage += validate(Objects::nonNull, response, "Response is null");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getForeignUpdate(), "Empty ForeignUpdate");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getStatus(), "Empty Status");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getID(), "Empty ID");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getActiveVehicleID(), "Empty ActiveVehicleID");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getActiveCompanyID(), "Empty ActiveCompanyID");
        MatcherAssert.assertThat("Incorrect UsersGetResponse response:\n" + errorMessage, errorMessage, isEmptyOrNullString());
    }

    public static void validateUsers(UsersPutResponse response, UsersPutRequest request) {
        String errorMessage = "";
        errorMessage += validate(Objects::nonNull, response, "Response is null");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getAccesstoken().getAccessToken(), "Empty accesstoken.access_token");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getAccesstoken().getRefreshToken(), "Empty accesstoken.refresh_token");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getAccesstoken().getUserId(), "Empty accesstoken.user_id");
        errorMessage += validate(r -> !StringUtils.isEmpty(r.toString()), response.getSession(), "Empty session");
        if(request != null) {
            errorMessage += validate(r -> r.equals(request.getCompanyID()), response.getAccesstoken().getScope(), "Incorrect accesstoken.scope");
        }
        MatcherAssert.assertThat("Incorrect UsersPutResponse response:\n" + errorMessage, errorMessage, isEmptyOrNullString());
    }


    private static String validate(Function<Object, Boolean> function, Object value, String errorMessage) {
        return !function.apply(value) ? String.format("%s: value = %s\n", errorMessage, value) : "";
    }
}
