package smoke;

import beans.api.request.common.GetStatusModelRequest;
import beans.api.response.common.GetStatusResponse;
import data.TestData;
import handlers.CommonHandler;
import org.testng.annotations.Test;

import static client.Method.GET_STATUS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class CommonSmokeTest {
    private CommonHandler commonHandler = new CommonHandler();
    private TestData testData = TestData.COMMON;

    @Test(description = "Get account status")
    public void checkGetStatus(){

        GetStatusModelRequest getStatusModelRequest = GetStatusModelRequest.builder(testData.get(GET_STATUS)).build();
        GetStatusResponse statusResponse = commonHandler.getStatus(getStatusModelRequest);

        assertThat("User status is 10", statusResponse.getUserStatus(), equalTo(10));
        assertThat("Balance is 0", statusResponse.getBalanceCoins(), is(0));
        assertThat("Insurance policy must be issued", statusResponse.getStatusInfo().getMessage(), equalTo("Оформить страховой полис"));
    }
}
