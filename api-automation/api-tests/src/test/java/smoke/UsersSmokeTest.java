package smoke;

import beans.api.request.users.UsersPostRequest;
import beans.api.request.users.UsersPutRequest;
import beans.api.response.users.UsersGetResponse;
import beans.api.response.users.UsersPostResponse;
import beans.api.response.users.UsersPutResponse;
import data.TestData;
import handlers.UsersHandler;
import org.testng.annotations.Test;
import utils.PhoneUtils;
import validation.UsersValidation;

import static client.Method.USERS_POST;
import static client.Method.USERS_PUT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Marina Buryak 24.10.2018
 */
public class UsersSmokeTest {
    private UsersHandler handler = new UsersHandler();
    private TestData testData = TestData.USERS;
    private String code;


    //-------------------------- POST -----------------------------------

    @Test(description = "Users (post): check 'access_token'")
    public void checkUsersPost() {
        UsersPostResponse response = handler.users_post();
        code = response.getAccesstoken().getRefreshToken();
        UsersValidation.validateUsers(response, null);
    }

    @Test(description = "Users (post): check 'scope'")
    public void checkUsersPost_Scope() {
        UsersPostRequest request = UsersPostRequest.builder(testData.get(USERS_POST)).phone(PhoneUtils.generateNumber()).build();
        UsersPostResponse response = handler.users_post(request);
        UsersValidation.validateUsers(response, request);
    }

    @Test(description = "Users (post): check error message", expectedExceptions = AssertionError.class,
            expectedExceptionsMessageRegExp = ".*Expected status code <200> doesn't match actual status code <500>.*")
    public void checkUsersPost_Error() {
        handler.users_post("incorrect_data");
    }

    //-------------------------- GET -----------------------------------

    @Test(description = "Users (get): check 'access_token'")
    public void checkUsersGet() {
        UsersGetResponse response = handler.users_get();
        UsersValidation.validateUsers(response);
    }

    //-------------------------- PUT -----------------------------------

    @Test(description = "Users (put): check 'access_token'", dependsOnMethods = "checkUsersPost")
    public void checkUsersPut() {
        UsersPutRequest request = UsersPutRequest.builder(testData.get(USERS_PUT)).code(code).build();
        UsersPutResponse response = handler.users_put(request);
        code = response.getAccesstoken().getRefreshToken();
        UsersValidation.validateUsers(response, request);
    }

    @Test(description = "Users (put): check 'scope'")
    public void checkUsersPut_Scope() {
        UsersPutRequest request = UsersPutRequest.builder(testData.get(USERS_PUT)).code(code).build();
        UsersPutResponse response = handler.users_put(request);
        code = response.getAccesstoken().getRefreshToken();
        UsersValidation.validateUsers(response, request);
    }

    @Test(description = "Users (put): check error (incorrect phone)", expectedExceptions = AssertionError.class,
            expectedExceptionsMessageRegExp = ".*Expected status code <200> doesn't match actual status code <500>.*")
    public void checkUsersPut_Error() {
        handler.users_put("incorrect_data");
    }

    @Test(description = "Users (put): check error message")
    public void checkUsersPut_ErrorMessage() {
        UsersPutRequest request = UsersPutRequest.builder(testData.get(USERS_PUT)).phone(PhoneUtils.generateNumber()).code(code).build();
        UsersPutResponse response = handler.users_put(request);

        assertThat("Incorrect error header", response.getInfo().getHeader(), equalTo("Внимание"));
        assertThat("Incorrect error message", response.getInfo().getMessage(), equalTo("Код введен неверно."));
    }
}
