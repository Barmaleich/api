package smoke;

import beans.api.request.vehicle_models.VehicleModelsRequest;
import beans.api.response.vehicle_models.VehicleModelsResponse;
import handlers.VehicleModelsHandler;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

/**
 * Created by Marina Buryak 31.10.2018
 */
public class VehicleModelsSmokeTest {
    private VehicleModelsHandler vehicleModelsHandler = new VehicleModelsHandler();

    @Test(description = "VehicleModels (get): with correct BranID")
    public void checkVehicleModels() {
        VehicleModelsRequest vehicleModelsRequest = new VehicleModelsRequest().toBuilder().brandID(46).build();
        List<VehicleModelsResponse> vehicleModelsResponse = vehicleModelsHandler.vehicleModels(vehicleModelsRequest);
        assertThat("Vehicle Models Response is not empty", vehicleModelsResponse, hasSize(greaterThan(1)));
    }


}
