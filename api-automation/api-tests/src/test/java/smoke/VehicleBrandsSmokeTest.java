package smoke;

import beans.api.request.vehicle_brands.VehicleBrandsRequest;
import beans.api.response.vehicle_brands.VehicleBrandsResponse;
import handlers.VehicleBrandsHandler;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

public class VehicleBrandsSmokeTest {
    private VehicleBrandsHandler brandsHandler = new VehicleBrandsHandler();

    @Test(description = "VehicleBrands (get): return brands list")
    public void chechVehicleBrands(){
        VehicleBrandsRequest brandsRequest = new VehicleBrandsRequest().toBuilder().build();
        List<VehicleBrandsResponse> brandsResponses = brandsHandler.vehicleBrands(brandsRequest);

        assertThat("Vehicle brands list size more than 10", brandsResponses, hasSize(greaterThan(10)));
//        assertThat("Vehicle brands list contains Acura", brandsResponses);
    }
}
