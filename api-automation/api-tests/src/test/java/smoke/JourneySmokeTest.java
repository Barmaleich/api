package smoke;

import beans.api.request.journey.JourneyStartPostRequest;
import beans.api.response.journey.JourneyStartResponse;
import client.Method;
import data.TestData;
import handlers.JourneyHandler;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class JourneySmokeTest {
    private JourneyHandler journeyHandler = new JourneyHandler();
    private TestData testData = TestData.JOURNEY;

    @Test(description = "Check that can start journey")
    public void checkStartJourney(){
        JourneyStartPostRequest journeyStartPostRequest = JourneyStartPostRequest.builder(testData.get(Method.JOURNEY_START)).build();
        JourneyStartResponse response = journeyHandler.journeyStart_post(journeyStartPostRequest);
        assertThat(response.getActionCode(), equalTo(0));
    }
}
