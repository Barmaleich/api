package config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Marina Buryak 24.10.2018
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Env {
    private static Env current;
    private String protocol = "http";
    private String host;
    private Integer port;

    public String getUrl() {
        return String.format("%s://%s%s", protocol, host, port == null ? "": ":" + port);
    }

    public static Env current() {
        if (current != null) return current;
        String env = System.getProperty("env");
        current = new Env();
        try {
            if (!StringUtils.isEmpty(env)) {
                env = env.startsWith("http") ? env : String.format("%s://%s",current.protocol, env);
                URL url = new URL(env);
                current.setProtocol(url.getProtocol());
                current.setHost(url.getHost());
                current.setPort(url.getPort() == -1 ? null : url.getPort());
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException(String.format("Incorrect set 'env' parameter(env = '%s')", env));
        }
        return current;
    }
}

