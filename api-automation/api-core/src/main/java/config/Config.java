package config;

import org.apache.commons.lang.StringUtils;

/**
 * Created by Marina Buryak 05.11.2018
 */

public enum Config {
    version ("version", null, true);

    private String name;
    private String value;
    private Boolean require;

    Config(String name, String defaultValue, Boolean required) {
        this.name = name;
        this.value = defaultValue;
        this.require = required;
    }

    public static void init() {
        for (Config param : Config.values()) {
            if (StringUtils.isEmpty(param.name)) {
                throw new RuntimeException("Parameter has empty name");
            }
            param.value = System.getProperty(param.name, param.value);
            if (StringUtils.isEmpty(param.value) && param.require) {
                throw new RuntimeException(String.format("Parameter '%s' is empty, but required", param.name));
            }
        }
    }

    public String getValue() {
        return value;
    }
}
