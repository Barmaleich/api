package client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.filter.Filter;
import com.jayway.restassured.filter.FilterContext;
import com.jayway.restassured.internal.mapping.Jackson2Mapper;
import com.jayway.restassured.internal.support.Prettifier;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.FilterableRequestSpecification;
import com.jayway.restassured.specification.FilterableResponseSpecification;
import com.jayway.restassured.specification.RequestSpecification;
import config.Config;
import config.Env;
import handlers.UsersHandler;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Created by Marina Buryak 23.10.2018
 */

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RestClient {
    @Getter
    private String env;
    private static RestClient restClient;
    private TokenFilter tokenFilter;

    public synchronized static RestClient createNewInstance() {
        return createNewInstance(Env.current().getUrl());
    }

    public synchronized static RestClient createNewInstance(String env) {
        log.info("Create rest client instance with env = '{}'", env);
        restClient = new RestClient();
        Config.init();
        restClient.env = env;
        restClient.createTokenFilter();
        return restClient;
    }

    public synchronized static RestClient getInstance() {
        return getInstance(Env.current().getUrl());
    }

    public synchronized static RestClient getInstance(String env) {
        log.info("Get rest client instance, env = '{}'", env);
        if(restClient == null) {
            restClient = createNewInstance(env);
        }
        restClient.env = env;
        return restClient;
    }

    @SuppressWarnings("all")
    public <T, F> T call(Method method, F request) {
            String url = env + method.name;
            RequestSpecification requestSpecification = RestAssured.given().contentType("application/json")
                    .queryParam("v", Config.version.getValue())
                    .filter(new Slf4JLoggingFilter())
                    .log().all(true).when();

            if(getTokenFilter() != null) {
                requestSpecification.filter(getTokenFilter());
            }

            Response response;
            switch (method.httpMethod) {
                case GET: {
                    response = requestSpecification.queryParameters(getParametersMap(request)).get(url);
                    break;
                }
                case POST: {
                    response = requestSpecification.body(getParameters(request)).post(url);
                    break;
                }
                case PUT: {
                    response = requestSpecification.body(getParameters(request)).put(url);
                    break;
                }
                case DELETE: {
                    response = requestSpecification.delete(url);
                    break;
                }
                case POST_URL:{
                    response = requestSpecification.queryParameters(getParametersMap(request)).post(url);
                }
                default: {
                    throw new RuntimeException(String.format("Incorrect http method name: '%s'", method.httpMethod.name()));
                }
            }
            response = response.then().statusCode(200).extract().response();

            return (T)response.as(method.responseClass, new Jackson2Mapper((aClass, s) -> {
                ObjectMapper m = new ObjectMapper();
                m.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
                return m.findAndRegisterModules();
            }));
    }


    private TokenFilter getTokenFilter() {
        return tokenFilter;
    }

    private void createTokenFilter() {
        this.tokenFilter = new TokenFilter(new UsersHandler().users_post().getAccesstoken().getAccessToken());
    }

    private static class Slf4JLoggingFilter implements Filter {
        @Override
        public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {
            Response response = ctx.next(requestSpec, responseSpec);
            log.info("\n\nResponse: \n{}\n", new Prettifier().getPrettifiedBodyIfPossible(response, response));
            return response;
        }
    }

    public static class TokenFilter implements Filter {

        private final AtomicReference<String> token = new AtomicReference<>();

        private TokenFilter(String tokenInResponse) {
            token.set(tokenInResponse);
        }

        public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {
            if (hasToken()) {
                requestSpec.auth().oauth2(token.get());
            }
            return ctx.next(requestSpec, responseSpec);
        }

        private boolean hasToken() {
            return isNotBlank(token.get());
        }
    }

    @SuppressWarnings("all")
    private <T> Map<String, String> getParametersMap(T request) {
        try {
            if(request == null) return new HashMap<>();
            return new ObjectMapper().readValue(getParameters(request), Map.class);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Impossible convert String to Map<String, String>: '%s' to json", request.getClass().getName()), e);
        }
    }

    private <T> String getParameters(T request) {
        try {
            if(request == null) return "";
            return new ObjectMapper().writeValueAsString(request);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(String.format("Impossible convert object '%s' to json", request.getClass().getName()), e);
        }
    }
}
