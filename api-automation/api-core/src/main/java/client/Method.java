package client;

import beans.api.request.common.GetStatusModelRequest;
import beans.api.request.journey.JourneyStartPostRequest;
import beans.api.request.users.UsersPostRequest;
import beans.api.request.users.UsersPutRequest;
import beans.api.request.vehicle_brands.VehicleBrandsRequest;
import beans.api.request.vehicle_models.VehicleModelsRequest;
import beans.api.response.common.GetStatusResponse;
import beans.api.response.journey.JourneyStartResponse;
import beans.api.response.users.UsersGetResponse;
import beans.api.response.users.UsersPostResponse;
import beans.api.response.users.UsersPutResponse;
import beans.api.response.vehicle_brands.VehicleBrandsResponse;
import beans.api.response.vehicle_models.VehicleModelsResponse;
import lombok.Getter;

import static client.Method.RequestMethod.*;

/**
 * Created by Marina Buryak 23.10.2018
 */
@Getter
public enum Method {
    USERS_GET("/api/Users", GET, UsersGetResponse.class),
    USERS_POST("/api/Users", POST, UsersPostRequest.class, UsersPostResponse.class),
    USERS_PUT("/api/Users", PUT, UsersPutRequest.class, UsersPutResponse.class),

    VEHICLE_MODELS("/api/VehicleModels", GET, VehicleModelsRequest.class, VehicleModelsResponse[].class),
    VEHICLE_BRANDS("/api/VehicleBrands", GET, VehicleBrandsRequest.class, VehicleBrandsResponse[].class),

    GET_STATUS("/api/Common/GetStatus", GET, GetStatusModelRequest.class, GetStatusResponse.class),
    JOURNEY_START("/api/Journey/Start", POST, JourneyStartPostRequest.class, JourneyStartResponse.class)
    ;

    Method(String name, RequestMethod method, Class<?> responseClass) {
        this(name, method, null, responseClass);
    }

    Method(String name, RequestMethod method, Class<?> requestClass, Class<?> responseClass) {
        this.name = name;
        this.httpMethod = method;
        this.requestClass = requestClass;
        this.responseClass = responseClass;
    }

    String name;
    int version;
    RequestMethod httpMethod;
    Class<?> requestClass;
    Class<?> responseClass;


    public enum RequestMethod {
        GET,
        POST,
        PUT,
        DELETE,
        POST_URL
    }
}
