package handlers;

import beans.api.request.vehicle_brands.VehicleBrandsRequest;
import beans.api.response.vehicle_brands.VehicleBrandsResponse;
import client.RestClient;
import data.TestData;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static client.Method.VEHICLE_BRANDS;

public class VehicleBrandsHandler {
    private RestClient restClient;

    public VehicleBrandsHandler(){
        restClient = RestClient.getInstance();
    }

    public VehicleBrandsHandler(RestClient restClient){
        this.restClient = restClient;
    }

    public List<VehicleBrandsResponse> vehicleBrands(VehicleBrandsRequest brandsRequest){
        return Arrays.asList(restClient.call(VEHICLE_BRANDS, brandsRequest));
    }
}
