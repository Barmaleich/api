package handlers;

import beans.api.request.journey.JourneyStartPostRequest;

import beans.api.response.journey.JourneyStartResponse;
import client.Method;
import client.RestClient;
import data.TestData;

public class JourneyHandler {
    private RestClient client;
    private TestData testData = TestData.JOURNEY;

    public JourneyHandler(){
        client = RestClient.getInstance();
    }

    public JourneyStartResponse journeyStart_post(JourneyStartPostRequest journeyStartPostRequest){
        return client.call(Method.JOURNEY_START, journeyStartPostRequest);
    }

    public JourneyStartResponse journeyStart_post(String nameTestData){
        JourneyStartPostRequest journeyStartPostRequest = testData.get(Method.JOURNEY_START, nameTestData);
        return journeyStart_post(journeyStartPostRequest);
    }
}
