package handlers;

import beans.api.request.vehicle_models.VehicleModelsRequest;
import beans.api.response.vehicle_models.VehicleModelsResponse;
import static client.Method.*;
import client.RestClient;
import data.TestData;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Marina Buryak 31.10.2018
 */
public class VehicleModelsHandler {
    private RestClient restClient;

    private TestData testData = TestData.USERS;

    public VehicleModelsHandler() {
        restClient = RestClient.getInstance();
    }

    public VehicleModelsHandler(RestClient restClient) {
        this.restClient = restClient;
    }

    public List<VehicleModelsResponse> vehicleModels(VehicleModelsRequest vehicleModelsRequest) {
        return Arrays.asList(restClient.call(VEHICLE_MODELS, vehicleModelsRequest));
    }

}
