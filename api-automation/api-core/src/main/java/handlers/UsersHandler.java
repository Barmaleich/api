package handlers;

import beans.api.request.users.UsersPostRequest;
import beans.api.request.users.UsersPutRequest;
import beans.api.response.users.UsersGetResponse;
import beans.api.response.users.UsersPostResponse;
import beans.api.response.users.UsersPutResponse;
import client.RestClient;
import data.TestData;

import static client.Method.*;

/**
 * Created by Marina Buryak 25.10.2018
 */

public class UsersHandler {
    private RestClient client;
    private TestData testData = TestData.USERS;

    public UsersHandler() {
        client = RestClient.getInstance();
    }

    public UsersHandler(RestClient restClient) {
        this.client = restClient;
    }

    public UsersPostResponse users_post(UsersPostRequest usersPostRequest) {
        return client.call(USERS_POST, usersPostRequest);
    }

    public UsersPostResponse users_post(String nameTestData) {
        UsersPostRequest usersPostRequest = testData.get(USERS_POST, nameTestData);
        return users_post(usersPostRequest);
    }

    public UsersPostResponse users_post() {
        UsersPostRequest usersPostRequest = testData.get(USERS_POST);
        return users_post(usersPostRequest);
    }

    public UsersGetResponse users_get() {
        return client.call(USERS_GET, null);
    }

    public UsersPutResponse users_put(UsersPutRequest usersPutRequest) {
        return client.call(USERS_PUT, usersPutRequest);
    }

    public UsersPutResponse users_put(String nameTestData) {
        UsersPutRequest usersPutRequest = testData.get(USERS_PUT, nameTestData);
        return users_put(usersPutRequest);
    }

    public UsersPutResponse users_put() {
        UsersPutRequest usersPutRequest = testData.get(USERS_PUT);
        return users_put(usersPutRequest);
    }
}
