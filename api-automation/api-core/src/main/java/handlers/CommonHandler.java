package handlers;

import beans.api.request.common.GetStatusModelRequest;
import beans.api.response.common.GetStatusResponse;
import client.Method;
import client.RestClient;
import data.TestData;

import static client.Method.GET_STATUS;

public class CommonHandler {
    private RestClient client;
    private TestData testData = TestData.COMMON;

    public CommonHandler(){
        client = RestClient.getInstance();
    }

    public CommonHandler(RestClient restClient){
        this.client = restClient;
    }

    public GetStatusResponse getStatus(GetStatusModelRequest getStatusModelsRequest){
        return client.call(GET_STATUS, getStatusModelsRequest);
    }

    public GetStatusResponse getStatus(String nameTestData){
        GetStatusModelRequest getStatusModelRequest = testData.get(GET_STATUS, nameTestData);
        return getStatus(getStatusModelRequest);
    }
}
