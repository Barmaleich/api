import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "BrandID",
    "Name",
    "MinYear",
    "bHidden"
})
public class Properties implements Serializable
{

    @JsonProperty("ID")
    private ID iD;
    @JsonProperty("BrandID")
    private BrandID brandID;
    @JsonProperty("Name")
    private Name name;
    @JsonProperty("MinYear")
    private MinYear minYear;
    @JsonProperty("bHidden")
    private BHidden bHidden;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3596256018306015890L;

    @JsonProperty("ID")
    public ID getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(ID iD) {
        this.iD = iD;
    }

    @JsonProperty("BrandID")
    public BrandID getBrandID() {
        return brandID;
    }

    @JsonProperty("BrandID")
    public void setBrandID(BrandID brandID) {
        this.brandID = brandID;
    }

    @JsonProperty("Name")
    public Name getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(Name name) {
        this.name = name;
    }

    @JsonProperty("MinYear")
    public MinYear getMinYear() {
        return minYear;
    }

    @JsonProperty("MinYear")
    public void setMinYear(MinYear minYear) {
        this.minYear = minYear;
    }

    @JsonProperty("bHidden")
    public BHidden getBHidden() {
        return bHidden;
    }

    @JsonProperty("bHidden")
    public void setBHidden(BHidden bHidden) {
        this.bHidden = bHidden;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("iD", iD).append("brandID", brandID).append("name", name).append("minYear", minYear).append("bHidden", bHidden).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(minYear).append(brandID).append(name).append(bHidden).append(iD).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Properties) == false) {
            return false;
        }
        Properties rhs = ((Properties) other);
        return new EqualsBuilder().append(minYear, rhs.minYear).append(brandID, rhs.brandID).append(name, rhs.name).append(bHidden, rhs.bHidden).append(iD, rhs.iD).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
