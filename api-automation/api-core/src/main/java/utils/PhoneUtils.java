package utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.Random;

/**
 * Created by Marina Buryak 25.10.2018
 */
public class PhoneUtils {

    public static String generateNumber() {
        return String.format("%s%s", RandomUtils.nextInt(1, 9), RandomStringUtils.randomNumeric(11));
    }
}
