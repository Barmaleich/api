package data;

import client.Method;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Marina Buryak 25.10.2018
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestData {
    public static final TestData USERS = create("data/users/users.json");
    public static final TestData COMMON = create("data/common/getstatus.json");
    public static final TestData JOURNEY = create("data/journey/journey.json");

    private String path;

    private TestData(String path) {
        this.path = path;
    }

    public static TestData create(String path) {
        return new TestData(path);
    }

    public static <T> T get(File file, Class<T> clazz, String namespace) {
        try {
            if (!file.exists()) {
                throw new RuntimeException("No file: " + file.getAbsolutePath());
            }
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(file);
            for (String tmp: namespace.split("\\.")) {
                node = node.get(tmp);
            }
            return mapper.treeToValue(node, clazz);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Impossible parse file '%s'('%s') to class %s", file.getName(), namespace, clazz.getName()), e);
        }
    }

    public <T> T get(Class<T> clazz, String namespaces) {
        URL url = this.getClass().getClassLoader().getResource(path);
        if(url == null) {
            throw new RuntimeException(String.format("File %s is absent", this.getClass().getClassLoader().getResource("/") + path));
        }
        return get(new File(url.getPath()), clazz, namespaces);
    }

    @SuppressWarnings("all")
    public <T> T get(Method method, String namespace) {
        return (T) get(method.getRequestClass(), method.getHttpMethod().name() + "." + namespace);
    }

    public <T> T get(Method method) {
        return get(method, "default");
    }
}
