package beans.api.request.vehicle_brands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(toBuilder=true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleBrandsRequest {

    private long id;
    public static VehicleBrandsRequestBuilder builder(VehicleBrandsRequest testData){
        return testData.toBuilder();
    }
}
