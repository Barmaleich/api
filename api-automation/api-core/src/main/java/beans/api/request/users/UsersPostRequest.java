
package beans.api.request.users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(toBuilder=true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersPostRequest
{
    private String phone;
    private Boolean silentMode;
    private String promoCode;
    private String companyID;
    private String companySecret;
    private String partnerID;
    private String partnerSecret;

    public static UsersPostRequestBuilder builder(UsersPostRequest testData) {
        return testData.toBuilder();
    }
}
