package beans.api.request.vehicle_models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Marina Buryak 31.10.2018
 */

@Builder(toBuilder=true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleModelsRequest {
    private Integer brandID;

    public static VehicleModelsRequestBuilder builder(VehicleModelsRequest testData) {
        return testData.toBuilder();
    }
}
