package beans.api.request.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(toBuilder=true)
@Data
@NoArgsConstructor
@AllArgsConstructor

public class GetStatusModelRequest {
    private double cityLat;
    private double cityLon;

    public static GetStatusModelRequestBuilder builder (GetStatusModelRequest testData){
        return testData.toBuilder();
    }
}
