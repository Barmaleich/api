package beans.api.request.journey;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(toBuilder=true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JourneyStartPostRequest {
    private double cityLat;
    private double cityLon;
    private Integer plannedDurationSeconds;
    private String timeZone;
    private Integer cityAccidentness;
    private String tariffMode;
    private Integer pushId;
    private Integer sessionId;

    public static JourneyStartPostRequestBuilder builder(JourneyStartPostRequest testData){
        return testData.toBuilder();
    }
}
