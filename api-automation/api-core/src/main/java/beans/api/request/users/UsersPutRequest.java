
package beans.api.request.users;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder(toBuilder=true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersPutRequest
{
    private String phone;
    private String code;
    private String companyID;
    private String companySecret;
    private Boolean silentMode;

    public static UsersPutRequestBuilder builder(UsersPutRequest testData) {
        return testData.toBuilder();
    }
}
